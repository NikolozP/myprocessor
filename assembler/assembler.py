import sys
from assemble import assemble

file_to_compile = sys.argv[1]
file_to_print_in = sys.argv[2]

file_to_compile = open(file_to_compile, 'r')
lines_to_compile = file_to_compile.readlines()
file_to_compile.close()

assembled_lines = assemble(lines_to_compile)

file_to_print_in = open(file_to_print_in, 'w')
for line in assembled_lines:
    file_to_print_in.write(line + '\n')
file_to_print_in.close()

assembled_lines_hex = []
for line in assembled_lines:
    to_add = hex(int(line, 2))[2:]
    while len(to_add) != 16:
        to_add = '0' + to_add
    assembled_lines_hex.append(to_add)

finalFile = open('ProgramForROM', 'w')
for line in assembled_lines_hex:
    finalFile.write(line + ' ')
finalFile.close()

print("DONE!")
