def assemble(lines):
    assembled = []
    lines = __enumerate_labels__(lines)
    for line in lines:
        compiled_line = __get_binary__(line.split())
        assembled.append(compiled_line)
    return assembled


__instructions__ = {
    "add": "0000000000000",
    "sub": "0000000000001",
    "mult": "0000000000010",
    "div": "0000000000011",
    "and": "0000000000100",
    "or": "0000000000101",
    "not": "0000000000110",
    "jmp": "0100000000000",
    "jgt": "0100000000001",
    "jlt": "0100000000010",
    "jeq": "0100000000011",
    "jne": "0100000000100",
    "jle": "0100000000101",
    "jge": "0100000000110",
    "sw": "1000000000000",
    "lw": "1000000000001",
    "move": "1000000000010"
}

__registers__ = {
    "R0": "0000000000000000",
    "R1": "0000000000000001",
    "R2": "0000000000000010",
    "R3": "0000000000000011",
    "RES": "0000000000000100",
    "SW": "0000000000000101",
    "LW": "0000000000000110"
}

__labels__ = {

}


def __get_binary__(words):
    inst = __instructions__[words[0]]
    register_or_value = ""
    for i in range(1, len(words)):
        register_or_value += ("1" if words[i].isnumeric() else "0")
    while len(register_or_value) < 3:
        register_or_value = "0" + register_or_value
    inst += register_or_value
    values = ""
    for i in range(1, len(words)):
        try:
            values += __registers__[words[i]]
        except KeyError:
            try:
                values += __dec_to_16bit_binary(words[i])
            except ValueError:
                values += __dec_to_16bit_binary(__labels__[words[i]])
    while len(values) < 48:
        values = '0' + values
    inst += values
    return inst


def __dec_to_16bit_binary(dec):
    binary = bin(int(dec))[2:]
    while len(binary) < 16:
        binary = '0' + binary
    return binary


def __enumerate_labels__(lines):
    new_lines = []
    i = 0
    k = 0
    for line in lines:
        if '(' in line:
            label = line[1:len(line) - 2]
            __labels__[label] = i - k
            k += 1
        else:
            new_lines.append(line)
        i += 1
    return new_lines
